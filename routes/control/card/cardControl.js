var format = require('../../util/stringFormatUtil.js');
var rules = require('../../util/rulesUtil.js');
var crypto = require('../../util/cryptoUtil.js');
var dateUtil = require('../../util/dateUtil.js');
var constants = require('../../util/constants.js');
var numberUtil = require('../../util/numberUtil.js');
var cardUtil = require('../../util/cardUtil.js');
var errorManagment = require('../../util/errorManagment.js');
var sessionManagment = require('../../util/sessionManagment.js');
var responseFactory = require('../../model/response/responseFactory.js');
var cardFactory = require('../../model/card/cardFactory.js');
var cardService = require('../../service/card/cardService.js');

exports.create = function(user, body, callback) {

  var mobile = format.getString(body.mobile);
  var alias = format.getString(body.alias);
  var imgCard = format.getString(body.imgCard);

  if(numberUtil.isCellPhone(mobile)){

    var card = getCard(user, mobile, alias, imgCard);

    listCards(user, card, callback);

  } else {
    return callback(errorManagment.getError("CRD01C", "Los datos ingresados no son correctos"));
  }
}

function listCards (user, card, callback){
  cardService.getCard (user._id.$oid, undefined, function(responseGetCard){
    if(responseGetCard.error.code==constants.CODE_SERVICE_SUCCESS ){
      var sameAccount = false;
      var noCredit = false;
      var numCards = 0;

      for(let i=0; i<responseGetCard.body.length; i++) {
        if (responseGetCard.body[i].status==="active"){
          numCards = numCards+1;

          if (responseGetCard.body[i].mobile==card.mobile ){
            sameAccount = true;
          } else if (responseGetCard.body[i].currenBalace.amount==0){
            noCredit = true;
          }
        }
      }

      if(numCards<constants.MAX_CARDS && !sameAccount && !noCredit){
        addCard(card, callback);
      } else if(numCards>=constants.MAX_CARDS){
        return callback(errorManagment.getError("CRD02C", "Solo se permite contar con " + constants.MAX_CARDS + " tarjetas"));
      } else if(sameAccount){
        return callback(errorManagment.getError("CRD03C", "El celular ingresado ya cuenta con una tarjeta asociada"));
      } else if(noCredit){
        return callback(errorManagment.getError("CRD04C", "Cuentas con una tarjeta en saldo en cero, no es posible crear una nueva"));
      } else {
        return callback(errorManagment.getGenericError());
      }
    } else {
      return callback(errorManagment.cloneError(responseGetCard));
    }
  });
}



function addCard (card, callback){
  cardService.createCard(card, function(respCreateCard){

    if(respCreateCard.error.code==constants.CODE_SERVICE_SUCCESS ){

      let response = responseFactory.newResponse();
      let cardResp = {};
      cardResp.number = card.number;
      cardResp.mobile = card.mobile;
      cardResp.alias = card.alias;
      cardResp.product = card.product;
      cardResp.imgCard = card.imgCard;
      cardResp.currenBalace = card.currenBalace;
      response.body = cardResp
      return callback(response);

    } else {
      return callback(errorManagment.cloneError(respCreateCard));
    }
  });
}



function getCard(user, mobile, alias, imgCard){
  var card = cardFactory.newCard();

  card.number =  cardUtil.generateNumberCard();
  card.mobile = mobile;
  card.alias = alias;
  card.status = constants.STATUS_CARD_ACTIVE;
  card.product = constants.NAME_PROD_CARD;
  card.creationDate = dateUtil.formatDateNow();
  card.imgCard = imgCard;
  card.userId = user._id.$oid;
  card.currenBalace.amount = 0;
  card.currenBalace.currency = constants.CURRENCY_MXN;
  card.expirationDate = getExpirationDate();
  return card;
}

function getExpirationDate(){
  let d = new Date();

  let y = (d.getFullYear()+5) + "";
  let m = d.getMonth()+1;

  if(m<10){
    return ("0"+m +""+ y.substring(2,5));
    console.log(res);
  } else {
    return (""+m +""+ y.substring(2,5));
  }
}