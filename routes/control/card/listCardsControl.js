var format = require('../../util/stringFormatUtil.js');
var rules = require('../../util/rulesUtil.js');
var crypto = require('../../util/cryptoUtil.js');
var dateUtil = require('../../util/dateUtil.js');
var constants = require('../../util/constants.js');
var numberUtil = require('../../util/numberUtil.js');
var cardUtil = require('../../util/cardUtil.js');
var errorManagment = require('../../util/errorManagment.js');
var sessionManagment = require('../../util/sessionManagment.js');
var responseFactory = require('../../model/response/responseFactory.js');
var cardFactory = require('../../model/card/cardFactory.js');
var cardService = require('../../service/card/cardService.js');

exports.getCards = function(session, callback) {

  cardService.getCard (session.user._id.$oid, undefined, function(responseGetCard){
    if(responseGetCard.error.code==constants.CODE_SERVICE_SUCCESS ){

      let response = responseFactory.newResponse();

      let cards = [];
      for(let i=0; i<responseGetCard.body.length; i++){
        let idCard = crypto.encodeMD5(responseGetCard.body[i]._id.$oid + responseGetCard.body[i].number + dateUtil.formatDateNow());
        responseGetCard.body[i].id=idCard;

        if(responseGetCard.body[i].status==="active"){
          let card = {};
          card.id = responseGetCard.body[i].id;
          card.number = responseGetCard.body[i].number;
          card.mobile = responseGetCard.body[i].mobile;
          card.alias = responseGetCard.body[i].alias;
          card.product = responseGetCard.body[i].product;
          card.imgCard = responseGetCard.body[i].imgCard;
          card.currenBalace = responseGetCard.body[i].currenBalace;
          card.expirationDate = responseGetCard.body[i].expirationDate;
          cards.push(card);
        }
      }

      sessionManagment.addCardsSession(session.idSession, responseGetCard.body);
      response.body = cards;
      return callback(response);

    } else {
      return callback(errorManagment.cloneError(responseGetCard));
    }
  });
}
