var format = require('../../util/stringFormatUtil.js');
var rules = require('../../util/rulesUtil.js');
var crypto = require('../../util/cryptoUtil.js');
var dateUtil = require('../../util/dateUtil.js');
var constants = require('../../util/constants.js');
var numberUtil = require('../../util/numberUtil.js');
var cardUtil = require('../../util/cardUtil.js');
var errorManagment = require('../../util/errorManagment.js');
var sessionManagment = require('../../util/sessionManagment.js');
var responseFactory = require('../../model/response/responseFactory.js');
var cardFactory = require('../../model/card/cardFactory.js');
var movementFactory = require('../../model/movement/movementFactory.js');

var movementService = require('../../service/movement/movementService.js');
var cardService = require('../../service/card/cardService.js');


exports.movements = function(session, idCard, callback) {
  let cardContext = undefined;

  if(session.cards!=undefined) {
    for(let i=0; i<session.cards.length; i++){
      if(session.cards[i].id==idCard) {
        cardContext = session.cards[i];
      }
    }

    if (cardContext!=undefined){
      try {
          getMovements (session, cardContext, callback);
      } catch (e) {
        console.log(e);
      } finally {

      }

    } else {
      return callback(errorManagment.getError("CRD001", "La tarjeta seleccionada no es valida"));
    }
  } else {
    return callback(errorManagment.getError("CRD001", "La tarjeta seleccionada no es valida"));
  }
}

function getMovements (session, cardContext, callback){
  movementService.getMovements(cardContext._id.$oid, undefined, function(responseMovements){
    if(responseMovements.error.code==constants.CODE_SERVICE_SUCCESS ){

      let response = responseFactory.newResponse();
      response.body = responseMovements.body;
      return callback(response);

    } else {
      return callback(responseMovements.cloneError(responseGetCard));
    }
  });
}
