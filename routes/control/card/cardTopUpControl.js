var format = require('../../util/stringFormatUtil.js');
var rules = require('../../util/rulesUtil.js');
var crypto = require('../../util/cryptoUtil.js');
var dateUtil = require('../../util/dateUtil.js');
var constants = require('../../util/constants.js');
var numberUtil = require('../../util/numberUtil.js');
var cardUtil = require('../../util/cardUtil.js');
var errorManagment = require('../../util/errorManagment.js');
var sessionManagment = require('../../util/sessionManagment.js');
var responseFactory = require('../../model/response/responseFactory.js');
var cardFactory = require('../../model/card/cardFactory.js');
var movementFactory = require('../../model/movement/movementFactory.js');

var movementService = require('../../service/movement/movementService.js');
var cardService = require('../../service/card/cardService.js');

exports.topUp = function(session, idCard, body, callback) {

  var creditCard = crypto.decodeBase64(body.numberCard);

  if (rules.isNumberCard(creditCard)) {
    if (rules.isExpirationCard(crypto.decodeBase64(body.expiration))) {
      if (rules.isCodeValid(crypto.decodeBase64(body.code))) {
        if(numberUtil.isPositive(body.amount)){

          var amount = parseFloat(body.amount);

          if(amount<=constants.MAX_TOP_UP){
            if(Math.random() > 0.3){
              addTopUp (session, idCard, creditCard, amount, callback)
            } else {
              return callback(errorManagment.getError("CRD-TO06", "Tu operacion fue declinada"));
            }
          } else {
            return callback(errorManagment.getError("CRD-TO05", "Por tu seguridad tu operación fue declinada"));
          }
        } else {
          return callback(errorManagment.getError("CRD-TO04", "El importe ingresado no es válido"));
        }
      } else {
        return callback(errorManagment.getError("CRD-TO03", "El formato de código de seguridad no es correcto"));
      }
    } else {
      return callback(errorManagment.getError("CRD-TO02", "La fecha de vencimiento no es válida"));
    }
  } else {
    return callback(errorManagment.getError("CRD-TO01", "La tarjeta de crédito ingresada no es válida"));
  }
}


function addTopUp (session, idCard, creditCard, amount, callback) {
  let cardContext = undefined;

  if(session.cards!=undefined){
    for(let i=0; i<session.cards.length; i++){
      if(session.cards[i].id==idCard) {
        cardContext = session.cards[i];
      }
    }

    if (cardContext!=undefined){
      getCard (session, cardContext, creditCard, amount, callback);
    } else {
      return callback(errorManagment.getError("CRD001", "La tarjeta seleccionada no es valida"));
    }
  } else {
    return callback(errorManagment.getError("CRD001", "La tarjeta seleccionada no es valida"));
  }
}

function getCard (session, card, creditCard, amount, callback) {
  cardService.getCard (session.user._id.$oid, card._id.$oid, function(responseGetCard){
      if(responseGetCard.error.code==constants.CODE_SERVICE_SUCCESS ){

        card = responseGetCard.body[0];
        let result = parseFloat(card.currenBalace.amount) + amount;
        let balance = result.toFixed(2);
        card.currenBalace.amount = balance;
        updateCard(session, card, creditCard, amount, callback);

      } else {
        return callback(errorManagment.cloneError(responseGetCard));
      }
  });
}

function updateCard (session, card, creditCard, amount, callback) {
  cardService.updateCard (card, function(responseGetCard){
    if(responseGetCard.error.code==constants.CODE_SERVICE_SUCCESS ){

      addMovement(session, card, creditCard, amount, callback);

    } else {
      return callback(errorManagment.cloneError(responseGetCard));
    }
  });
}

function addMovement (session, card, creditCard, amount, callback){
  var movement = getMovement (session, card, creditCard, amount);

  movementService.createMovement(movement, function (responseCreateMovement){
    if(responseCreateMovement.error.code==constants.CODE_SERVICE_SUCCESS ){
      let response = responseFactory.newResponse();
      let cardResp = {};
      cardResp.number = card.number;
      cardResp.mobile = card.mobile;
      cardResp.alias = card.alias;
      cardResp.product = card.product;
      cardResp.imgCard = card.imgCard;
      cardResp.currenBalace = card.currenBalace;
      response.body = cardResp
      return callback(response);

    } else {
      return callback(errorManagment.cloneError(responseCreateMovement));
    }
  });
}

function getMovement (session, card, creditCard, amount){
  let movement = movementFactory.newMovement();
  let operationDate = dateUtil.formatDateNow();
  movement.concept = "Recarga de tarjeta *****" + creditCard.substring(12,16);
  movement.transactionType.id = constants.TRANSACTION_TYPE_TOP_UP_CODE;
  movement.transactionType.name = constants.TRANSACTION_TYPE_TOP_UP_DESC;
  movement.operationDate = operationDate;
  movement.cardId = card._id.$oid;
  movement.cardNumber =  card.number;
  movement.cardMobile = card.mobile;
  movement.localAmount.amount = amount
  movement.localAmount.currency = constants.CURRENCY_MXN;

  return movement;
}
