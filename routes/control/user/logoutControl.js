var format = require('../../util/stringFormatUtil.js');
var rules = require('../../util/rulesUtil.js');
var crypto = require('../../util/cryptoUtil.js');
var dateUtil = require('../../util/dateUtil.js');
var errorManagment = require('../../util/errorManagment.js');
var sessionManagment = require('../../util/sessionManagment.js');
var responseFactory = require('../../model/response/responseFactory.js');
var userFactory = require('../../model/user/userFactory.js');
var userService = require('../../service/user/userService.js');

exports.logout = function(idUser, idSession, callback) {
  var response = responseFactory.newResponse();
  response.body = {};
  response.body.message="Tu sesión ha finalizado"

  let email = crypto.decodeBase64(idUser);

  if(idSession!=undefined){
      sessionManagment.invalidateSession(idSession, email);
  }

  return callback(response);
}
