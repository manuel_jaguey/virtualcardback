var format = require('../../util/stringFormatUtil.js');
var rules = require('../../util/rulesUtil.js');
var crypto = require('../../util/cryptoUtil.js');
var dateUtil = require('../../util/dateUtil.js');
var constants = require('../../util/constants.js');
var errorManagment = require('../../util/errorManagment.js');
var sessionManagment = require('../../util/sessionManagment.js');
var responseFactory = require('../../model/response/responseFactory.js');
var userFactory = require('../../model/user/userFactory.js');
var userService = require('../../service/user/userService.js');

exports.changePassword = function(emailUser, body, callback) {
  let email = format.getStringToLowerCase(crypto.decodeBase64(emailUser));
  let password = crypto.encodeMD5(crypto.decodeBase64(format.getString(body.password)));
  let newPassword = crypto.decodeBase64(format.getString(body.newPassword));

  if(!rules.validatePassword(newPassword)){
    return callback(errorManagment.getError("USR03C", "El formato de la nueva contraseña no es válida"));

  } else if(rules.validateEmail(email)){
    var newPass = crypto.encodeMD5(crypto.decodeBase64(format.getString(body.newPassword)));

    if(newPass===password){
      return callback(errorManagment.getError("USR03C", "La nueva contraseña debe ser disferente a la anterior"));

    } else {
        updatePassword(email, password, newPass, callback);
    }
  } else {
    return callback(errorManagment.getError("USR03C", "El correo y/o contraseña no son válidos"));

  }
}

function updatePassword (email, password, newPassword, callback){
  userService.getUser(email, undefined, function(respGetUser){

    console.log(JSON.stringify(respGetUser));

    if(respGetUser.error.code==constants.CODE_SERVICE_SUCCESS){
      let user = respGetUser.body[0];

      if(password==user.password){
        setPassword(user, newPassword, callback);
      } else {
        addAttempt(user, callback);
      }

    } else if(respGetUser.error.code==constants.CODE_SERVICE_CONECTION_FAIL){
      return callback(errorManagment.cloneError(respGetUser));

    } else {
      return callback(errorManagment.getError("USR04C", "El correo y/o contraseña no son válidos"));
    }
  });
}

function setPassword(user, newPassword, callback){
  user.password = newPassword;
  user.session.attempts=0;
  user.session.blocked=false;
  user.session.sessiondateBlocked = "";

  userService.updateUser(user, function(respUpdateUser){

    if(respUpdateUser.error.code==constants.CODE_SERVICE_SUCCESS){
      let response = getReponse(user);
      return callback(response);

    } else {
      return callback(errorManagment.getError("USR04C", "Servicio temporalmente no disponible, intenta más tarde"));
    }
  });
}


function addAttempt(user, callback){
  if(user.session.blocked){
    return callback(errorManagment.getError("USR04C", "Por tu seguridad tu sesión ha sido bloqueada por " +sessionManagment.getBlockingHours()+ " horas"));

  } else {
    user.session.attempts = user.session.attempts+1;
    if(user.session.attempts==3){
      user.session.blocked=true;
      user.session.sessiondateBlocked = dateUtil.formatDateNow();
    }

    userService.updateUser(user, function(respUpdateUser){
      if(user.session.blocked){
        return callback(errorManagment.getError("USR04C", "Por tu seguridad tu sesión ha sido bloqueada por " +sessionManagment.getBlockingHours()+ " horas"));
      } else {
        return callback(errorManagment.getError("USR04C", "El correo y/o contraseña no son válidos"));
      }
    });
  }
}

function getReponse(user){
  let response = responseFactory.newResponse(undefined);
  response.body={};
  response.body.name = {};
  response.body.name.firtsName = user.name.firtsName;
  response.body.changePassword = "SUCCESS"
  return response;
}
