var format = require('../../util/stringFormatUtil.js');
var rules = require('../../util/rulesUtil.js');
var crypto = require('../../util/cryptoUtil.js');
var dateUtil = require('../../util/dateUtil.js');
var constants = require('../../util/constants.js');
var errorManagment = require('../../util/errorManagment.js');
var sessionManagment = require('../../util/sessionManagment.js');
var responseFactory = require('../../model/response/responseFactory.js');
var userFactory = require('../../model/user/userFactory.js');
var userService = require('../../service/user/userService.js');

exports.createUser = function(body, callback) {
  let resp = responseFactory.newResponse();

  let firtsName = format.getStringToUpperCase(body.name.firtsName);
  let fatherSurname = format.getStringToUpperCase(body.name.fatherSurname);
  let motherSurname = format.getStringToUpperCase(body.name.motherSurname);
  let email = format.getStringToLowerCase(body.email);
  let password = crypto.decodeBase64(format.getString(body.password));
  let birthDate = format.getString(body.birthDate);

  console.log("firtsName : " + firtsName +" = "+ rules.noSpecialCharsNotEmpty(firtsName)
    + "\nfatherSurname : " + fatherSurname +" = "+ rules.noSpecialCharsNotEmpty(fatherSurname)
    + "\nmotherSurname : " + motherSurname +" = "+ (motherSurname==="" || rules.noSpecialCharsNotEmpty(motherSurname))
    + "\nemail : " + email +" = "+ rules.validateEmail(email)
    + "\nbirthDate : " + birthDate +" = "+ rules.validDate(birthDate)
    + "\npassword : " + password +" = "+ rules.validatePassword(password));

  if(rules.noSpecialCharsNotEmpty(firtsName) && rules.noSpecialCharsNotEmpty(fatherSurname)
    && (motherSurname==="" || rules.noSpecialCharsNotEmpty(motherSurname))
    && rules.validateEmail(email) && rules.validDate(birthDate)){

      let user = userFactory.newUser();
      user.name.firtsName = firtsName;
      user.name.fatherSurname = fatherSurname;
      user.name.motherSurname = motherSurname;
      user.birthDate =  dateUtil.formatDate(birthDate);
      user.email = email;
      user.password = crypto.encodeMD5(password);
      user.imageUser = "";
      user.lastAccess = "";
      user.creationDate = dateUtil.formatDateNow();
      user.active = true;

      userService.getUser(user.email, undefined, function(respGetUser){
        if(respGetUser.error.code==constants.CODE_SERVICE_SUCCESS && respGetUser.body.length>0){
            resp.error.code = "USR01C";
            resp.error.message = "Los datos ingresados ya exisen";
            return callback(resp);

        } else if(respGetUser.error.code==constants.CODE_SERVICE_CONECTION_FAIL){
          return callback(respGetUser);

        } else {
            userService.createUser(user, function(respCreateUser){
              if(respGetUser.error.code==constants.CODE_SERVICE_SUCCESS){
                resp.body = {};
                return callback(respGetUser);
              } else {
                return callback(respCreateUser);
              }
          });
        }
      });
  } else {
    resp.error.code = "USR02C";
    resp.error.message = "Los datos ingresados no son correctos, favor de validar.";
    return callback(resp);
  }
}
