var format = require('../../util/stringFormatUtil.js');
var rules = require('../../util/rulesUtil.js');
var crypto = require('../../util/cryptoUtil.js');
var dateUtil = require('../../util/dateUtil.js');
var constants = require('../../util/constants.js');
var errorManagment = require('../../util/errorManagment.js');
var sessionManagment = require('../../util/sessionManagment.js');
var responseFactory = require('../../model/response/responseFactory.js');
var userFactory = require('../../model/user/userFactory.js');
var userService = require('../../service/user/userService.js');

exports.loginUser = function(emailUser, body, res, callback) {
  let email = format.getStringToLowerCase(crypto.decodeBase64(emailUser));
  let password = crypto.encodeMD5(crypto.decodeBase64(format.getString(body.password)));

  if(rules.validateEmail(email)){
    getUser(email, password, res, callback);
  } else {
    return callback(errorManagment.getError("USR03C", "El correo y/o contraseña no son válidos"));
  }
}

function getUser (email, password, res, callback){
  userService.getUser(email, undefined, function(respGetUser){

    if(respGetUser.error.code==constants.CODE_SERVICE_SUCCESS && respGetUser.body.length>0 ){
      let user = respGetUser.body[0];

      if(password==user.password){
        getSession(user, res, callback);
      } else {
        addAttempt(user, callback);
      }

    } else if(respGetUser.error.code==constants.CODE_SERVICE_CONECTION_FAIL){
      return callback(errorManagment.cloneError(respGetUser));

    } else {
      return callback(errorManagment.getError("USR04C", "El correo y/o contraseña no son válidos"));
    }
  });
}

function getSession(user, res, callback){
  if(user.session.blocked &&
    ((sessionManagment.getDiference(new Date(user.session.sessiondateBlocked), new Date())) < (sessionManagment.getBlockingHours()*60))){
    return callback(errorManagment.getError("USR04C", "Por tu seguridad tu sesión ha sido bloqueada por " +sessionManagment.getBlockingHours()+ " horas"));

  } else {
    let initSession = dateUtil.dateNow();

    console.log(initSession);
    console.log(dateUtil.formatDateNow());

    let lastAccess = user.lastAccess;
    let session = sessionManagment.createSession(user, initSession, res);

    if(session.idSession != undefined){
      user.lastAccess = dateUtil.formatDate(initSession);
      user.session.attempts=0;
      user.session.blocked=false;
      user.session.sessiondateBlocked = "";

      userService.updateUser(user, function(respUpdateUser){

        if(respUpdateUser.error.code==constants.CODE_SERVICE_SUCCESS){
          let response = responseFactory.newResponse(session.idSession);
          response.body = getReponseLogin(user,lastAccess);
          return callback(response);
        } else {
          return callback(errorManagment.cloneError(respUpdateUser));
        }
      });
    } else {
      return callback(errorManagment.getError(session.code, session.message));
    }
  }
}

function addAttempt(user, callback){
  if(user.session.blocked){
    return callback(errorManagment.getError("USR04C", "Por tu seguridad tu sesión ha sido bloqueada por " +sessionManagment.getBlockingHours()+ " horas"));

  } else {
    user.session.attempts = user.session.attempts+1;
    if(user.session.attempts==3){
      user.session.blocked=true;
      user.session.sessiondateBlocked = dateUtil.formatDateNow();
    }

    userService.updateUser(user, function(respUpdateUser){
      if(user.session.blocked){
        return callback(errorManagment.getError("USR04C", "Por tu seguridad tu sesión ha sido bloqueada por " +sessionManagment.getBlockingHours()+ " horas"));
      } else {
        return callback(errorManagment.getError("USR04C", "El correo y/o contraseña no son válidos"));
      }
    });
  }
}

function getReponseLogin(user,lastAccess){
  let response = userFactory.userResponse();
  response.name.firtsName = user.name.firtsName
  response.name.fatherSurname = user.name.fatherSurname
  response.lastAccess = lastAccess;
  response.imageUser = user.imageUser;
  response.idUser = crypto.ecodeBase64(user.email);
  return response;
}
