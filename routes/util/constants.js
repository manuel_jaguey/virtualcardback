/*
* CONSTANTS
*/
module.exports = {
    //CONNECTION
    CONN_URL_BASE : "https://api.mlab.com/api/1/databases/virtual-card",
    CONN_API_KEY : "P2FwaUtleT1tWjJreEVTelc5TThBRTUzWU5yODdWVDNsSWs2dWw5cw==",
    CONN_COLLECTION_USER : "/collections/user",
    CONN_COLLECTION_CARD :"/collections/card",
    CONN_COLLECTION_MOVEMENTS : "/collections/movements",

    //CODE SERVICES
    CODE_SERVICE_CONECTION_FAIL : "CON00S",
    CODE_SERVICE_EXECUTION_ERROR : "EXE00S",
    CODE_SERVICE_SUCCESS : "SUC00S",

    //PRODUCTS
    NAME_PROD_CARD : "Virual Card",

    //STATUS CARD
    STATUS_CARD_ACTIVE : "active",
    STATUS_CARD_BLOCKED : "blocked",
    STATUS_CARD_CANCELED: "canceled",

    //CARDS
    MAX_CARDS : 3,

    //currency
    CURRENCY_MXN : "MXN",

    //TOP UP
    MAX_TOP_UP : 5000,

    //transaction Type
    TRANSACTION_TYPE_TOP_UP_CODE : "TYTO",
    TRANSACTION_TYPE_TOP_UP_DESC : "top up"
};
