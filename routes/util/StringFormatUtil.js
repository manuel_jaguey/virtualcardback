
exports.getString = function(data) {
  if(data==undefined){
    return "";
  } else {
    return data.trim();
  }
}

exports.getStringToUpperCase = function(data) {
  if(data==undefined){
    return "";
  } else {
    return data.trim().toUpperCase();
  }
}

exports.getStringToLowerCase = function(data) {
  if(data==undefined){
    return "";
  } else {
    return data.trim().toLowerCase();
  }
}
