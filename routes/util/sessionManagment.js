
var crypto = require('../util/cryptoUtil.js');
var dateUtil = require('../util/dateUtil.js');
var sessionFactory = require('../model/session/session.js');

var sessions = [];

var maxInactivityTime = 15; //regresar a 15
var maxActivityTime = 60;
var blockingHours = 2;

exports.createSession = function(user, initSession, res){
  let resp = sessionFactory.responseSession();

  cleanSession(user._id.$oid);

  let activeSession = findSession(user._id.$oid);

  if(activeSession.length>0){
    resp.idSession = undefined;
    resp.code = "SDS001";
    resp.message = "Tu sesión anterior no cerro correctamente, espera " +maxInactivityTime+ " minutos y vuelve a intentarlo";
  } else {
    let session = addSession(user, initSession);
    resp.idSession = session.idSession;

    setHeaders(res, resp.idSession, undefined);
  }
  return resp;
}

exports.invalidateSession = function(idSession, email){
  removeSession(idSession, email);
}

exports.getSession = function (idSession, res){
  let resp = sessionFactory.responseSession();
  let session = findUser(idSession);

  if(session==undefined){
    resp.idSession = undefined;
    resp.code = "SDS002";
    resp.message = "Tu sesión no es válida o ha expirado, ingresa nuevamente";
  } else {
    let timeSession = diferenceTime(session.initSession, new Date());
    let inactivityTime =  diferenceTime(session.lastRequest, new Date());

    setHeaders(res, session.idSession, (maxActivityTime-timeSession));

    if(inactivityTime>=maxInactivityTime){
      resp.idSession = undefined;
      resp.code = "SDS003";
      resp.message = "Tu sesión ha expirado, ingresa nuevamente";
      removeSession(idSession, session.user.email);

    } else if(timeSession>=maxActivityTime){
      resp.idSession = undefined;
      resp.code = "SDS003";
      resp.message = "Tu sesión ha expirado, ingresa nuevamente";
      removeSession(idSession, session.user.email);
    } else {
      updateLastRequest(idSession);
      resp.idSession = session.idSession;
      resp.user = session.user;
    }
  }
  return resp;
}

exports.getSessionContext = function (idSession, res){
  let resp = {};
  let session = findUser(idSession);

  if(session==undefined){
    resp.idSession = undefined;
    resp.code = "SDS002";
    resp.message = "Tu sesión no es válida o ha expirado, ingresa nuevamente";
  } else {
    let timeSession = diferenceTime(session.initSession, new Date());
    let inactivityTime =  diferenceTime(session.lastRequest, new Date());

    setHeaders(res, session.idSession, (maxActivityTime-timeSession));
    
    if(inactivityTime>=maxInactivityTime){
      resp.idSession = undefined;
      resp.code = "SDS003";
      resp.message = "Tu sesión ha expirado, ingresa nuevamente";
      removeSession(idSession, session.user.email);

    } else if(timeSession>=maxActivityTime){
      resp.idSession = undefined;
      resp.code = "SDS003";
      resp.message = "Tu sesión ha expirado, ingresa nuevamente";
      removeSession(idSession, session.user.email);

    } else {
      updateLastRequest(idSession);
      resp = session;
    }
  }
  return resp;
}

exports.addCardsSession = function (idSession, cards){
  for(let i = sessions.length - 1; i >= 0; i--) {
    if(idSession==sessions[i].idSession){
      sessions[i].cards = cards;
    }
  }
}



exports.getBlockingHours = function (){
  return blockingHours;
}

exports.getDiference = function (initTime, endTime){
  return diferenceTime(initTime, endTime);
}

function updateLastRequest (idSession){
  for(let i = sessions.length - 1; i >= 0; i--) {
    if(idSession==sessions[i].idSession){
      sessions[i].lastRequest = new Date();
    }
  }
}

function findUser(idSession){
  for(let i = sessions.length - 1; i >= 0; i--) {
    if(idSession==sessions[i].idSession){
      return sessions[i];
    }
  }
}

function cleanSession (idUser){
  for(let i = sessions.length - 1; i >= 0; i--) {
    if(idUser==sessions[i].idUser){
      let inactivityTime = diferenceTime(sessions[i].lastRequest, new Date());
      if(inactivityTime>=maxInactivityTime){
        sessions.splice(i, 1);
      }
    }
  }
}

function removeSession (idSession, email) {

  for(let i = sessions.length - 1; i >= 0; i--) {
    console.log(idSession +" : "+ sessions[i].idSession +" - "+ email +" : "+ sessions[i].user.email);
    if(idSession==sessions[i].idSession && email==sessions[i].user.email){
      sessions.splice(i, 1);
    }
  }
}

function diferenceTime(initTime, endTime){
	var diff = endTime-initTime;
	var diffMins = Math.round(((diff % 86400000) % 3600000) / 60000);
	return diffMins;
}

function addSession (user, initSession){
  let session = sessionFactory.create();
  session.idUser = user._id.$oid;
  session.initSession = initSession;
  session.lastRequest = initSession;
  session.idSession = crypto.encodeMD5(user._id.$oid + user.email + initSession);
  session.user = user;
  session.cards = [];
  sessions.push(session);

  return session;
}

function findSession(idUser) {
  return sessions.filter(function(session) {
      return (session.idUser===idUser);
  });
}


function setHeaders(res, idSession, remainingTime){
  if(remainingTime!=undefined){
    res.setHeader('remaining-time', remainingTime);
  }
    
  res.setHeader('session-id', idSession);
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Accept-Language', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET,POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,session-id,remaining-time');
  res.setHeader('Access-Control-Expose-Headers', 'X-Requested-With,content-type,session-id,remaining-time');
  res.setHeader('Access-Control-Allow-Credentials', true);
}
