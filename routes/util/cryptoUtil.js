var crypto = require('crypto');

exports.ecodeBase64 = function(data) {
  try {
      if(data!=undefined && data.trim()!=""){
          return new Buffer(data).toString('base64');
      } else {
          return undefined;
      }
  } catch (e) {
    return undefined;
  }
}

exports.decodeBase64 = function(data) {
  try {
    return new Buffer(data, 'base64').toString('ascii');
  } catch (e) {
    return undefined;
  }
}

exports.encodeMD5 = function (data) {
  try {
    return crypto.createHash('md5').update(data).digest("hex");
  } catch (e) {
    return undefined;
  }

}
