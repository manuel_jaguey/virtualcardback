
/*
* YYYY-MM-DD
*/
exports.formatDate = function(date) {
  try {
    var d = new Date(date);
    return d.toJSON();
  } catch (e) {
    return undefined;
  }
}

exports.formatDateNow = function() {
  try {
    var d = new Date();
    d.setTime( d.getTime() + d.getTimezoneOffset()*60*1000 );
    return d.toJSON();
  } catch (e) {
    return undefined;
  }
}

exports.dateNow = function() {
  try {
    var d = new Date();
    d.setTime( d.getTime() + d.getTimezoneOffset()*60*1000 );
    return d.toJSON();
  } catch (e) {
    return undefined;
  }
}
