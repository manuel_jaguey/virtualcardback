var responseFactory = require('../model/response/responseFactory.js');

exports.cloneError = function(response) {
  let raesponseError = responseFactory.newResponse();
  raesponseError.error.code = response.error.code;
  raesponseError.error.message = response.error.message;
  return raesponseError;
}

exports.getError = function(code, message) {
  let raesponseError = responseFactory.newResponse();
  raesponseError.error.code = code;
  raesponseError.error.message = message;
  return raesponseError;
}

exports.getGenericError = function(code, message) {
  return responseFactory.newResponse();
}
