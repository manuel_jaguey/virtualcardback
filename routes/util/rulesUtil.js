

exports.noSpecialCharsNotEmpty = function (value){
  return /(^[a-z0-9\xf1\. ]{3,25})+$/i.test(value);
}

exports.validateEmail = function (value) {
  var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return reg.test(value);
}

exports.validatePassword = function (value) {
  return /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(value);
}

exports.validDate = function (value) {
  var reg = /^\d{4}[./-]\d{2}[./-]\d{2}$/
  return reg.test(value);
}

exports.isNumberCard = function (card) {
  var isDigits =  /^([0-9]){16}$/.test(card);
 return isDigits;
}

exports.isCodeValid = function (code){
  var isDigits =  /^([0-9]){3}$/.test(code);
 return isDigits;
}

exports.isExpirationCard = function (expiration){
  var isDigits =  /^([0-9]){4}$/.test(expiration);

  if(isDigits){
    try {
      var month = parseInt(expiration.substring(0,2));
      var year = parseInt(expiration.substring(2,4));
      return (month>0 && month<=12 && year > 0);
    } catch (e){
      return false;
    }
 } else {
   return isDigits;
 }
}
