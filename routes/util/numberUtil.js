/*
* isNumber
*/
exports.isNumber = function(number) {
   return /^\d/.test(number);
}

exports.isRational = function(number) {
   return /^-?(0|[1-9]\d*)(\.\d+)?$/.test(number);
}
exports.isCellPhone = function (number){
  return /^\d{10}$/.test(number)
}

exports.isPositive = function(number) {
  return /^(0|[1-9]\d*)(\.\d+)?$/.test(number);
}

