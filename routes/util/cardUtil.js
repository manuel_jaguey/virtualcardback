
var dateUtil = require('../util/dateUtil.js');

exports.generateNumberCard = function() {
  var d = dateUtil.formatDateNow();

  var numberCard =
    "7" //Lucky Number
    + d.substring(2, 4)
    + d.substring(5, 7)
    + d.substring(8, 10)
    + d.substring(11, 13)
    + d.substring(14, 16)
    + d.substring(17, 19)
    + d.substring(20, 23);

    return numberCard;
}
