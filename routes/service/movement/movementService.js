var requestJson = require('request-json');
var responseFactory = require('../../model/response/responseFactory.js');

var crypto = require('../../util/cryptoUtil.js');
var constants = require('../../util/constants.js');

exports.createMovement = function(movement, callback){
  let response = responseFactory.newResponse();

  let _urlCreateMovement = constants.CONN_URL_BASE + constants.CONN_COLLECTION_MOVEMENTS+ crypto.decodeBase64(constants.CONN_API_KEY);
  console.log("POST: " + _urlCreateMovement +"\n"+ JSON.stringify(movement));

  let client = requestJson.createClient(_urlCreateMovement);

  client.post('', movement, function(err,res,body){
    if (err){
      response.error.code = constants.CODE_SERVICE_CONECTION_FAIL;
      return callback(response);
    } else {
        if(res.statusCode==200){
          response.body = {};
          response.body = body;
          response.error.code = constants.CODE_SERVICE_SUCCESS;
          return callback(response);
        } else {
          response.error.code = constants.CODE_SERVICE_EXECUTION_ERROR + res.statusCode;
          return callback(response);
        }
    }
  });
}

exports.getMovements = function(cardId, movementId, callback){
  let response = responseFactory.newResponse();

  var filter = {};

  if(cardId!=undefined){
    filter.card = {};
    filter.cardId = cardId;
  }

  if(movementId!=undefined){
    filter._id = {};
    filter._id.$oid = movementId;
  }

  var _urlGetMovement = constants.CONN_URL_BASE + constants.CONN_COLLECTION_MOVEMENTS
      + crypto.decodeBase64(constants.CONN_API_KEY)  + "&q=" + JSON.stringify(filter);
  console.log("GET: " + _urlGetMovement);

  var client = requestJson.createClient(_urlGetMovement);

  client.get('', function(err,res,body){
    if (err){
      response.error.code = constants.CODE_SERVICE_CONECTION_FAIL;
      return callback(response);
    } else {
        if(res.statusCode==200){
          response.body = {};
          response.body = body;
          response.error.code = constants.CODE_SERVICE_SUCCESS;
          return callback(response);
        } else {
          response.error.code = constants.CODE_SERVICE_EXECUTION_ERROR + res.statusCode;
          return callback(response);
        }
    }
  });
}

exports.updateMovement = function(movement, callback){
  let response = responseFactory.newResponse();

  let _urlUpdateMovement = constants.CONN_URL_BASE + constants.CONN_COLLECTION_MOVEMENTS
      +"/"+ movement._id.$oid + crypto.decodeBase64(constants.CONN_API_KEY);
  console.log("PUT: " + _urlUpdateMovement +"\n"+ JSON.stringify(movement));

  let client = requestJson.createClient(_urlUpdateMovement);

  client.put('', movement, function(err,res,body){
    if (err){
      response.error.code = constants.CODE_SERVICE_CONECTION_FAIL;
      return callback(response);
    } else {
        if(res.statusCode==200){
          response.body = {};
          response.body = body;
          response.error.code = constants.CODE_SERVICE_SUCCESS;
          return callback(response);
        } else {
          response.error.code = constants.CODE_SERVICE_EXECUTION_ERROR + res.statusCode;
          return callback(response);
        }
    }
  });
}
