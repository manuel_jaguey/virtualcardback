var requestJson = require('request-json');
var responseFactory = require('../../model/response/responseFactory.js');

var crypto = require('../../util/cryptoUtil.js');
var constants = require('../../util/constants.js');

exports.createCard = function(card, callback){
  let response = responseFactory.newResponse();

  let _urlCreateCard = constants.CONN_URL_BASE + constants.CONN_COLLECTION_CARD+ crypto.decodeBase64(constants.CONN_API_KEY);
  console.log("POST: " + _urlCreateCard +"\n"+ JSON.stringify(card));

  let client = requestJson.createClient(_urlCreateCard);

  client.post('', card, function(err,res,body){
    if (err){
      response.error.code = constants.CODE_SERVICE_CONECTION_FAIL;
      return callback(response);
    } else {
        if(res.statusCode==200){
          response.body = {};
          response.body = body;
          response.error.code = constants.CODE_SERVICE_SUCCESS;
          return callback(response);
        } else {
          response.error.code = constants.CODE_SERVICE_EXECUTION_ERROR + res.statusCode;
          return callback(response);
        }
    }
  });
}

exports.getCard = function(userId, cardId, callback){
  let response = responseFactory.newResponse();

  var filter = {};

  if(userId!=undefined){
    filter.userId = userId;
  }

  if(cardId!=undefined){
    filter._id = {};
    filter._id.$oid = cardId;
  }

  var _urlGetCard = constants.CONN_URL_BASE + constants.CONN_COLLECTION_CARD
      + crypto.decodeBase64(constants.CONN_API_KEY)  + "&q=" + JSON.stringify(filter);
  console.log("GET: " + _urlGetCard);

  var client = requestJson.createClient(_urlGetCard);

  client.get('', function(err,res,body){
    if (err){
      response.error.code = constants.CODE_SERVICE_CONECTION_FAIL;
      return callback(response);
    } else {
        if(res.statusCode==200){
          response.body = {};
          response.body = body;
          response.error.code = constants.CODE_SERVICE_SUCCESS;
          return callback(response);
        } else {
          response.error.code = constants.CODE_SERVICE_EXECUTION_ERROR + res.statusCode;
          return callback(response);
        }
    }
  });
}

exports.updateCard = function(card, callback){
  let response = responseFactory.newResponse();

  let _urlUpdateCard = constants.CONN_URL_BASE + constants.CONN_COLLECTION_CARD
      +"/"+ card._id.$oid + crypto.decodeBase64(constants.CONN_API_KEY);
  console.log("PUT: " + _urlUpdateCard +"\n"+ JSON.stringify(card));

  let client = requestJson.createClient(_urlUpdateCard);

  client.put('', card, function(err,res,body){
    if (err){
      response.error.code = constants.CODE_SERVICE_CONECTION_FAIL;
      return callback(response);
    } else {
        if(res.statusCode==200){
          response.body = {};
          response.body = body;
          response.error.code = constants.CODE_SERVICE_SUCCESS;
          return callback(response);
        } else {
          response.error.code = constants.CODE_SERVICE_EXECUTION_ERROR + res.statusCode;
          return callback(response);
        }
    }
  });
}
