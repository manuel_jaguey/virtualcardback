var requestJson = require('request-json');
var responseFactory = require('../../model/response/responseFactory.js');

var crypto = require('../../util/cryptoUtil.js');
var constants = require('../../util/constants.js');

exports.createUser = function(user, callback){
  let response = responseFactory.newResponse();

  let _urlCreateUser = constants.CONN_URL_BASE + constants.CONN_COLLECTION_USER+ crypto.decodeBase64(constants.CONN_API_KEY);
  console.log("POST: " + _urlCreateUser +"\n"+ JSON.stringify(user));

  let client = requestJson.createClient(_urlCreateUser);

  client.post('', user, function(err,res,body){
    if (err){
      response.error.code = constants.CODE_SERVICE_CONECTION_FAIL;
      return callback(response);
    } else {
        if(res.statusCode==200){
          response.body = {};
          response.body = body;
          response.error.code = constants.CODE_SERVICE_SUCCESS;
          return callback(response);
        } else {
          response.error.code = constants.CODE_SERVICE_EXECUTION_ERROR + res.statusCode;
          return callback(response);
        }
    }
  });
}

exports.getUser = function(email, password, callback){
  let response = responseFactory.newResponse();

  var filter = {};
  filter.email=email;

  if(password!=undefined){
    filter.password = password;
  }

  var _urlGetUser = constants.CONN_URL_BASE + constants.CONN_COLLECTION_USER
      + crypto.decodeBase64(constants.CONN_API_KEY)  + "&q=" + JSON.stringify(filter);
  console.log("GET: " + _urlGetUser);

  var client = requestJson.createClient(_urlGetUser);

  client.get('', function(err,res,body){
    if (err){
      response.error.code = constants.CODE_SERVICE_CONECTION_FAIL;
      return callback(response);
    } else {
        if(res.statusCode==200){
          response.body = {};
          response.body = body;
          response.error.code = constants.CODE_SERVICE_SUCCESS;
          return callback(response);
        } else {
          response.error.code = constants.CODE_SERVICE_EXECUTION_ERROR + res.statusCode;
          return callback(response);
        }
    }
  });
}

exports.updateUser = function(user, callback){
  let response = responseFactory.newResponse();
  
  let _urlUpdateUser = constants.CONN_URL_BASE + constants.CONN_COLLECTION_USER
      +"/"+ user._id.$oid + crypto.decodeBase64(constants.CONN_API_KEY);
  console.log("PUT: " + _urlUpdateUser +"\n"+ JSON.stringify(user));

  let client = requestJson.createClient(_urlUpdateUser);

  client.put('', user, function(err,res,body){
    if (err){
      response.error.code = constants.CODE_SERVICE_CONECTION_FAIL;
      return callback(response);
    } else {
        if(res.statusCode==200){
          response.body = {};
          response.body = body;
          response.error.code = constants.CODE_SERVICE_SUCCESS;
          return callback(response);
        } else {
          response.error.code = constants.CODE_SERVICE_EXECUTION_ERROR + res.statusCode;
          return callback(response);
        }
    }
  });
}
