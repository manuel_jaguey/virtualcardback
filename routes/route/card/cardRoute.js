var express = require('express');
var router = express.Router();
var sessionManagment = require('../../util/sessionManagment.js');
var errorManagment = require('../../util/errorManagment.js');
var cardControl = require('../../control/card/cardControl.js');
var listCardsControl = require('../../control/card/listCardsControl.js');
var cardTopUpControl = require('../../control/card/cardTopUpControl.js');
var movementsControl = require('../../control/card/movementsControl.js');

/* Create card */
router.post('/v1/card', function(req, res, next) {
  var session = sessionManagment.getSession(req.headers['session-id'], res);

  if(session.idSession != undefined){

    cardControl.create(session.user, req.body, function(response){
      if (response!=undefined && response.body!=undefined){
          res.send(response.body);
      } else {
        return res.status(400).json(response.error);
      }
    });

  } else {
    return res.status(400).json((errorManagment.getError(session.code, session.message)).error);
  }
});


/*List cards*/
router.get('/v1/cards/', function(req, res, next) {
  var session = sessionManagment.getSessionContext(req.headers['session-id'], res);

  if(session.idSession != undefined){

    listCardsControl.getCards(session, function(response){
      if (response!=undefined && response.body!=undefined){
          res.send(response.body);
      } else {
        return res.status(400).json(response.error);
      }
    });

  } else {
    return res.status(400).json((errorManagment.getError(session.code, session.message)).error);
  }
});


/*topUp card*/
router.put('/v1/cards/:idCard/topUp', function(req, res, next) {
  let session = sessionManagment.getSessionContext(req.headers['session-id'], res);
  let idCard = req.params.idCard;

  if(session.idSession != undefined){

    cardTopUpControl.topUp(session, idCard, req.body, function(response){
      if (response!=undefined && response.body!=undefined){
          res.send(response.body);
      } else {
        return res.status(400).json(response.error);
      }
    });

  } else {
    return res.status(400).json((errorManagment.getError(session.code, session.message)).error);
  }
});

/*topUp card*/
router.get('/v1/cards/:idCard/movements', function(req, res, next) {
  let session = sessionManagment.getSessionContext(req.headers['session-id'], res);
  let idCard = req.params.idCard;

  if(session.idSession != undefined){

    movementsControl.movements(session, idCard, function(response){
      if (response!=undefined && response.body!=undefined){
          res.send(response.body);
      } else {
        return res.status(400).json(response.error);
      }
    });
  } else {
    return res.status(400).json((errorManagment.getError(session.code, session.message)).error);
  }
});


module.exports = router;
