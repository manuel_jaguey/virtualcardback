var express = require('express');
var router = express.Router();
var userControl = require('../../control/user/userControl.js');
var loginControl = require('../../control/user/loginControl.js');
var logoutControl = require('../../control/user/logoutControl.js');
var password  = require('../../control/user/password.js');
var sessionManagment = require('../../util/sessionManagment.js');
var errorManagment = require('../../util/errorManagment.js');

/* CreateUser */
router.post('/v1/user', function(req, res, next) {

  userControl.createUser(req.body, function(response){

    if (response!=undefined && response.body!=undefined){
        res.send(response.body);
    } else {
      return res.status(400).json(response.error);
    }
  });
});

/* Login */
router.post('/v1/user/:user/login', function(req, res, next) {

  loginControl.loginUser(req.params.user, req.body, res, function(response){

    if (response!=undefined && response.body!=undefined){
        res.send(response.body);
    } else {
      return res.status(400).json(response.error);
    }
  });
});

/* Logout */
router.get('/v1/user/:user/logout', function(req, res, next) {
  let idUser = req.params.user;
  let idSession = req.headers['session-id'];

  logoutControl.logout(idUser, idSession, function(response){
    if (response!=undefined && response.body!=undefined){
        res.send(response.body);
    } else {
      return res.status(400).json(response.error);
    }
  });
});

/* Change password */
router.put('/v1/user/:user/password', function(req, res, next) {
  let idUser = req.params.user;
  var session = sessionManagment.getSession(req.headers['session-id'], res);

  if(session.idSession != undefined){

    password.changePassword(idUser, req.body, function(response){
      if (response!=undefined && response.body!=undefined){
          res.send(response.body);
      } else {
        return res.status(400).json(response.error);
      }
    });

  } else {
    return res.status(400).json((errorManagment.getError(session.code, session.message)).error);
  }
});


module.exports = router;
