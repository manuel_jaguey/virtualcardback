/*
* Layout
*/
exports.newMovement = function(session) {
  return {
    concept : undefined,
    transactionType : {
      id : undefined,
      name : undefined
    },
    operationDate : undefined,
    card : {
      id : undefined,
      number : undefined,
      mobile : undefined
    },
    localAmount : {
      amount : undefined,
      currency : undefined
    }
  }
}
