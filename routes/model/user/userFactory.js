/*
* Layout Usuario
*/
exports.newUser = function() {
  return {
    name: {
      firtsName : null,
      fatherSurname : null,
      motherSurname : null
    },
    birthDate : null,
    email : null,
    password: null,
    imageUser : null,
    lastAccess : null,
    creationDate: null,
    active: true,
    session: {
      blocked: false,
      attempts: 0,
      dateBlocked: null
    }
  }
}

exports.userResponse = function() {
  return {
    idUser: null,
    name: {
      firtsName : null,
      fatherSurname : null
    },
    imageUser : null,
    lastAccess : null
  }
}
