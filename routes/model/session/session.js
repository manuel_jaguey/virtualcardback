/*
* Layout Session
*/
exports.create = function() {
  return {
    idSession: undefined,
    initSession: undefined,
    lastRequest: undefined,
    idUser: undefined,
    cards: undefined,
    user: {}
  }
}

exports.responseSession = function (){
  return {
    idSession: undefined,
    message: undefined,
    code: undefined,
    cards: undefined,
    user: {}
  }
}
