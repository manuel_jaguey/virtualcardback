/*
* Layout respuesta service
*/
exports.newResponse = function(session) {
  return {
    body : undefined,
    idSession: session,
    error :{
      code: "WSG001",
      message: "Servicio temporalmente no disponible"
    },
    user: null
  };
}
