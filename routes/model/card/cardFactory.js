
exports.newCard = function() {
  return {
    number: null,
    mobile: null,
    alias: null,
    status: null,
    product: null,
    creationDate: null,
    imgCard: null,
    userId: null,
    currenBalace: {
      amount: null,
      currency: null
    },
    historyCards: [],
    expirationDate: null
  }
}
